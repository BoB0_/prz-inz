import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class EventService {

  private subjects: Map<string, Subject<any>>;

  constructor() {
    this.subjects = new Map();
    this.subjects.set('REQUEST_SENT', new Subject());
    this.subjects.set('RESPONSE_RECEIVED', new Subject());
  }

  public next(event: string): void {
    this.subjects.get(event).next();
  }

  public get(event: string): Subject<any> {
    return this.subjects.get(event);
  }
}
