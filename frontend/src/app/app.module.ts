import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import { ContentComponent } from './components/content/content.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormComponent } from './components/form/form.component';
import { FileSelectDirective } from 'ng2-file-upload';
import { ResultComponent } from './components/result/result.component';
import { LoaderComponent } from './components/loader/loader.component';
import { LocalStorageModule, LocalStorageService } from 'angular-2-local-storage';
import {EventService} from '../services/event.service';

@NgModule({
  declarations: [
    FileSelectDirective,
    AppComponent,
    WrapperComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    FormComponent,
    ResultComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    FormsModule,
    HttpModule,
    LocalStorageModule.withConfig({
      prefix: 'prz-inz',
      storageType: 'sessionStorage'
    })
  ],
  providers: [
    EventService,
    LocalStorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
