export class Circle {
  private x: number;
  private y: number;
  private radius: number;
  public active: number;

  private canvasContext: any;


  constructor(x: number, y: number, radius: number, canvasContext: any) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.canvasContext = canvasContext;
  }

  public draw(): void {
    if (this.active > 0) {
      this.canvasContext.beginPath();
      this.canvasContext.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
      this.canvasContext.fillStyle = 'rgba(0,0,0,' + this.active + ')';
      this.canvasContext. fill();
    }
  }
}
