import { Circle } from './circle.model';
export interface IPoint {
  x: number;
  originX: number;
  y: number;
  originY: number;
  closest: IPoint[];
  circle: Circle;
  active: number;
}
