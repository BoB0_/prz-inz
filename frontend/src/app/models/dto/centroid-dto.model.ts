export interface ICentroidDTO {
  id: string;
  attributesValues: number[];
  group: number;
  baseClass: number;
}
