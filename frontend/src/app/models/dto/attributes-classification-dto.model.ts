import {IDataDTO} from './data-dto.model';
import {IAttributeDTO} from './attribute-dto.model';
export interface IAttributesClassificationDTO {
  id: string;
  chosenData: IDataDTO;
  dataList: IDataDTO[];
  attributes: IAttributeDTO[];
  distanceMeasure: number;
  elementsCount: number;
}
