import {IElementDTO} from "./element-dto.model";
import {IElementClassDTO} from "./element-class-dto.model";
import {ICentroidDTO} from "./centroid-dto.model";
export interface IDataDTO {
  id: string;
  centroidsCount: number;
  attributes: string[];
  elements: IElementDTO[];
  elementClasses: IElementClassDTO[];
  centroids: ICentroidDTO[];
  oldCentroids: ICentroidDTO[];
  elementsTestGroups: IElementDTO[][];
  currentTestGroup: IElementDTO[];
  errors: number[];
  testsErrors: number[];
  avgError: number[];
  avgTestError: number[];
}
