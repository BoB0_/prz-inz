export interface IElementDTO {
  id: string;
  attributesValues: number[];
  centroidGroup: number;
  baseClass: number;
  givenClass: number;
}
