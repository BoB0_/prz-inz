export interface IElementClassDTO {
  id: string;
  baseClass: number;
  numberOfElementsWithThisClass: number;
}
