export interface IAttributeDTO {
  id: string;
  name: string;
  magnitude: number;
  avgError: number;
}
