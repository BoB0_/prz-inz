import {Component, OnDestroy, OnInit} from '@angular/core';
import {EventService} from '../../../services/event.service';
import {LocalStorageService} from 'angular-2-local-storage/dist';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit, OnDestroy {

  public gotResponse = false;
  private enableResult = false;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private eventService: EventService,
              private localStorage: LocalStorageService) {

    this.eventService.get('REQUEST_SENT').takeUntil(this.ngUnsubscribe).subscribe(() => this.showResult());
    this.eventService.get('RESPONSE_RECEIVED').takeUntil(this.ngUnsubscribe).subscribe(() => this.responseReceived());
  }

  public ngOnInit(): void {
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public showResult(): void {
    this.enableResult = true;
    this.gotResponse = false;
  }

  public responseReceived(): void {
    this.gotResponse = true;
  }
}
