import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IPoint } from '../../models/point.model';
import { Circle } from '../../models/circle.model';
import { TweenMax } from 'gsap/TweenMax';

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.scss']
})
export class WrapperComponent implements OnInit {

  @ViewChild('backgroundCanvas') canvas: ElementRef;
  @ViewChild('background') background: ElementRef;

  private height: number;
  private width: number;
  private canvasContext: any;
  private target: IPoint = {
    x: null,
    originX: null,
    y: null,
    originY: null,
    closest: null,
    circle: null,
    active: null
  };

  private points: IPoint[] = [];

  constructor() {
  }

  public ngOnInit(): void {
    this.onResize();
    this.initCanvas();
    this.initPoints();
    this.registerListeners();
    this.initAnimation();
  }

  public initCanvas(): void {
    this.canvas.nativeElement.height = this.height;
    this.canvas.nativeElement.width = this.width;
    this.canvasContext = this.canvas.nativeElement.getContext('2d');
  }

  public initPoints(): void {
    for (let x = 0; x < this.width; x = x + this.width / 20) {
      for (let y = 0; y < this.height; y = y + this.height / 20) {
        let px = x + Math.random() * this.width / 20;
        let py = y + Math.random() * this.height / 20;
        let p = { x: px, originX: px, y: py, originY: py, closest: null, circle: null, active: null };
        this.points.push(p);
      }
    }

    // for each point find the 5 closest points
    for (let i = 0; i < this.points.length; i++) {
      let closest: IPoint[] = [];
      let p1 = this.points[i];

      for (let j = 0; j < this.points.length; j++) {
        let p2 = this.points[j];

        if (p1 !== p2) {
          let placed = false;

          for (let k = 0; k < 5; k++) {
            if (!placed) {
              if (closest[k] === undefined) {
                closest[k] = p2;
                placed = true;
              }
            }
          }

          for (let k = 0; k < 5; k++) {
            if (!placed) {
              if (this.getDistance(p1, p2) < this.getDistance(p1, closest[k])) {
                closest[k] = p2;
                placed = true;
              }
            }
          }
        }
      }
      p1.closest = closest;
    }

    // assign a circle to each point
    this.points.forEach(p => {
      p.circle = new Circle(p.x, p.y, 2 + Math.random() * 2, this.canvasContext);
    });
  }

  public initAnimation(): void {
    this.animate();
    this.points.forEach(p => this.shiftPoint(p));
  }

  public animate(): void {
    this.canvasContext.clearRect(0, 0, this.width, this.height);

    this.points.forEach(p => {
      let distance = Math.abs(this.getDistance(this.target, p));
      if (distance < 4000) {
        p.active = 0.6;
        p.circle.active = 0.8;
      } else if (distance < 20000) {
        p.active = 0.3;
        p.circle.active = 0.45;
      } else if (distance < 40000) {
        p.active = 0.1;
        p.circle.active = 0.2;
      } else {
        p.active = 0;
        p.circle.active = 0;
      }

      this.drawLines(p);
      p.circle.draw();
    });
    requestAnimationFrame(() => this.animate());
  }

  public shiftPoint(point: IPoint): void {
    let rand: number = 1 + Math.random();
    let newX: number = point.originX - 50 + Math.random() * 100;
    let newY: number = point.originY - 50 + Math.random() * 100;

    TweenMax.to(point.circle, rand, {
      x: newX,
      y: newY,
      ease: TweenMax.Circ.easeInOut
    });

    TweenMax.to(point, rand, {
      x: newX,
      y: newY,
      ease: TweenMax.Circ.easeInOut,
      onComplete: () => this.shiftPoint(point)
    });
  }

  public drawLines(point: IPoint): void {
    if (point.active && point.active > 0) {
      point.closest.forEach(p => {
        this.canvasContext.beginPath();
        this.canvasContext.moveTo(point.x, point.y);
        this.canvasContext.lineTo(p.x, p.y);
        this.canvasContext.strokeStyle = 'rgba(0,0,0,' + p.active + ')';
        this.canvasContext.stroke();
      });
    }
  }

  private getDistance(p1: IPoint, p2: IPoint): number {
    return Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2);
  }

  private registerListeners(): void {
    if (!('ontouchstart' in window)) {
      this.background.nativeElement.addEventListener('mousemove', this.mouseMove.bind(this), false);
    }
    this.background.nativeElement.addEventListener('scroll', this.onScroll.bind(this), false);
  }

  private mouseMove(event: MouseEvent): void {
    let posX = 0;
    let posY = 0;

    if (event.pageX || event.pageY) {
      posX = event.pageX;
      posY = event.pageY;
    } else if (event.clientX || event.clientY) {
      posX = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      posY = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }

    this.target.x = posX;
    this.target.y = posY;
  }

  private onScroll(): void {
    // this.animateHeader = document.body.scrollTop <= this.height;
  }

  public onResize(): void {
    this.height = this.background.nativeElement.clientHeight;
    this.width = this.background.nativeElement.clientWidth;
    this.canvas.nativeElement.height = this.height;
    this.canvas.nativeElement.width = this.width;
  }

}
