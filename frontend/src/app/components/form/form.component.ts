import {Component, OnInit} from '@angular/core';
import {Http, Headers, URLSearchParams} from '@angular/http';

import 'rxjs/add/operator/map';
import {LocalStorageService} from 'angular-2-local-storage/dist';
import {EventService} from '../../../services/event.service';
import {IAttributesClassificationDTO} from "../../models/dto/attributes-classification-dto.model";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  public distanceMeasure: number = 0;
  public centroids = 1;
  private file: File;
  public fileName = 'Brak pliku ...';

  constructor(private http: Http,
              private localStorage: LocalStorageService,
              private eventService: EventService) {
  }

  ngOnInit() {
  }

  public getFile(event: any): void {
    let fileList: FileList = event.target.files;

    if (fileList.length > 0) {
      this.file = event.target.files[0];
      this.fileName = this.file.name;
    }
  }

  public doUpload(): void {
    this.localStorage.set('file_name', this.fileName);
    this.eventService.next('REQUEST_SENT');
    let formData: FormData = new FormData();
    formData.append('file', this.file, this.file.name);
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    let params = new URLSearchParams();
    params.set('centroids', this.centroids.toString());

    this.http.post('http://localhost:8080/api/attributes_classification',
      formData, {headers: headers, params: params})
      .map(response => response.json())
      .subscribe(
        response => {
          this.localStorage.set('response', response as IAttributesClassificationDTO);
          this.eventService.next('RESPONSE_RECEIVED');
        },
        error => {

        }
      );
  }
}
