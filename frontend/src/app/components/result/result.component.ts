import {Component, OnDestroy, OnInit} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {EventService} from '../../../services/event.service';
import {Subject} from 'rxjs/Subject';
import {IAttributesClassificationDTO} from '../../models/dto/attributes-classification-dto.model';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit, OnDestroy {

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    animateScale: true,
    legend: {
      position: 'left'
    }
  };
  public barChartType = 'pie';
  public barChartLabels: string[] = [];
  public barChartLegend = true;
  public barChartData: number[] = [];
  public barChartColors: string[] = [];

  public loading = true;
  public showResult = false;
  public attributesClassification: IAttributesClassificationDTO;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  public chosenDataCentroids: number;
  public attributeColorMap: Map<string, string> = new Map<string, string>();
  public fileName: string = '';

  constructor(private localStorage: LocalStorageService,
              private eventService: EventService) {
    this.eventService.get('RESPONSE_RECEIVED').takeUntil(this.ngUnsubscribe).subscribe(() => this.showResponse());
    this.eventService.get('REQUEST_SENT').takeUntil(this.ngUnsubscribe).subscribe(() => this.requestSent());
  }

  public ngOnInit(): void {

  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public requestSent(): void {
    this.showResult = false;
    this.loading = true;
    this.attributesClassification = null;
    this.chosenDataCentroids = null;
    this.attributeColorMap = new Map<string, string>();

    this.barChartColors = [];
    this.barChartData = [];
    this.barChartLabels = [];
  }

  public showResponse(): void {
    if (this.loading) {
      this.loading = false;
    }
    this.showResult = true;
    this.fileName = this.localStorage.get('file_name') as string;
    this.attributesClassification = this.localStorage.get('response') as IAttributesClassificationDTO;
    this.chosenDataCentroids = this.attributesClassification.chosenData.centroidsCount;
    this.fixAttributesMagnitude();
    this.sortResponseElements();
    this.addBarChartLabels();
    this.addBarChartData();
  }

  private fixAttributesMagnitude(): void {
    this.attributesClassification.attributes.forEach(a => {
      a.avgError = parseFloat(a.avgError.toFixed(2));
      a.magnitude = parseFloat(a.magnitude.toFixed(2));
    });
  }

  private sortResponseElements(): void {
    this.attributesClassification.attributes.sort((a1, a2) => {
      if (a1.magnitude > a2.magnitude) {
        return -1;
      } else if (a1.magnitude === a2.magnitude) {
        return 0;
      } else {
        return 1;
      }
    });
  }

  private addBarChartLabels(): void {
    this.attributesClassification.attributes.forEach(a => {
      this.barChartLabels.push(a.name);
      let color = this.getRandomColor();
      this.attributeColorMap.set(a.name, color);
      this.barChartColors.push(color);
    });
  }

  private getRandomColor(): string {
    let letters = '0123456789ABCDEF'.split('');
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }

    return color;
  }

  private addBarChartData(): void {
    this.attributesClassification.attributes.forEach(a => {
      this.barChartData.push(a.magnitude);
    });
  }
}
