import { PrzInzPage } from './app.po';

describe('prz-inz App', () => {
  let page: PrzInzPage;

  beforeEach(() => {
    page = new PrzInzPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
