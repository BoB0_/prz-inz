package com.rbobko.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rbobko.exception.CloneFailedException;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Super Rafał on 17.04.2017.
 */
@Document
public class Centroid implements Cloneable {

    @Id
    private String id;
    @JsonIgnore
    private List<Double> attributesValues;
    private int group;
    private int baseClass;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Double> getAttributesValues() {
        return attributesValues;
    }

    public void setAttributesValues(List<Double> attributesValues) {
        this.attributesValues = attributesValues;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getBaseClass() {
        return baseClass;
    }

    public void setBaseClass(int baseClass) {
        this.baseClass = baseClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Centroid centroid = (Centroid) o;
        int equalityCount = 0;

        for (int i = 0; i < this.attributesValues.size(); i++) {
            if (Objects.equals(this.attributesValues.get(i),
                    centroid.getAttributesValues().get(i))) {
                equalityCount++;
            }
        }

        return equalityCount == this.attributesValues.size();

    }

    @Override
    public Object clone() {
        try {
            Centroid centroid = (Centroid) super.clone();

            if(attributesValues != null){
                centroid.attributesValues = new ArrayList<>();
                this.getAttributesValues().forEach(av -> centroid.getAttributesValues().add(new Double(av.doubleValue())));
            }

            return centroid;
        } catch (CloneNotSupportedException e) {
            throw new CloneFailedException("Error encountered while cloning element");
        }
    }
}
