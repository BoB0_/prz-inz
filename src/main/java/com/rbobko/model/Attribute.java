package com.rbobko.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Super Rafał on 15.05.2017.
 */
@Document
public class Attribute {

    @Id
    private String id;
    private String name;
    private Double magnitude;
    private Double avgError;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(Double magnitude) {
        this.magnitude = magnitude;
    }

    public Double getAvgError() {
        return avgError;
    }

    public void setAvgError(Double avgError) {
        this.avgError = avgError;
    }
}
