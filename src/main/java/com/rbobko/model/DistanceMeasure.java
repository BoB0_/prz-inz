package com.rbobko.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

/**
 * Created by Super Rafał on 23.05.2017.
 */
public enum DistanceMeasure {
    EUKLIDES(0),
    CANBERR(1),
    CZYBYSZEW(2),
    MANHATTAN(3);

    private int value;

    private DistanceMeasure(int value){
        this.value = value;
    }

    @JsonValue
    public int getValue(){
        return this.value;
    }

    @JsonCreator
    public static DistanceMeasure create(int value){
        return Arrays.stream(DistanceMeasure.values())
                .filter(type -> type.getValue() == value)
                .findFirst()
                .orElse(EUKLIDES);
    }
}
