package com.rbobko.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rbobko.exception.CloneFailedException;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Super Rafał on 17.04.2017.
 */
@Document
public class Element implements Cloneable {

    @Id
    private String id;
    @JsonIgnore
    private List<Double> attributesValues;
    private int centroidGroup;
    private int baseClass;
    private int givenClass;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Double> getAttributesValues() {
        return attributesValues;
    }

    public void setAttributesValues(List<Double> attributesValues) {
        this.attributesValues = attributesValues;
    }

    public int getCentroidGroup() {
        return centroidGroup;
    }

    public void setCentroidGroup(int centroidGroup) {
        this.centroidGroup = centroidGroup;
    }

    public int getBaseClass() {
        return baseClass;
    }

    public void setBaseClass(int baseClass) {
        this.baseClass = baseClass;
    }

    public int getGivenClass() {
        return givenClass;
    }

    public void setGivenClass(int givenClass) {
        this.givenClass = givenClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Element element = (Element) o;

        return element.getId().equals(this.getId());
    }

    @Override
    public int hashCode() {
        int result = attributesValues.hashCode();
        result = 31 * result + baseClass;
        return result;
    }

    @Override
    public Object clone() {
        try {
            Element element = (Element) super.clone();

            element.setId(this.getId());
            element.setBaseClass(this.getBaseClass());
            element.setGivenClass(this.getGivenClass());
            element.setCentroidGroup(this.getCentroidGroup());
            if(attributesValues != null){
                element.attributesValues = new ArrayList<>(this.attributesValues);
            }

            return element;
        } catch (CloneNotSupportedException e) {
            throw new CloneFailedException("Error encountered while cloning element");
        }
    }
}
