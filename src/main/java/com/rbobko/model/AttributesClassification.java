package com.rbobko.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Super Rafał on 15.05.2017.
 */
@Document
public class AttributesClassification {

    @Id
    private String id;
    private Data chosenData;
    private int elementsCount;
    private List<Data> dataList;
    private List<Attribute> attributes;
    private DistanceMeasure distanceMeasure;

    public AttributesClassification(){
        this.dataList = new ArrayList<>();
        this.attributes = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Data getChosenData() {
        return chosenData;
    }

    public void setChosenData(Data chosenData) {
        this.chosenData = chosenData;
    }

    public List<Data> getDataList() {
        return dataList;
    }

    public void setDataList(List<Data> dataList) {
        this.dataList = dataList;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public DistanceMeasure getDistanceMeasure() {
        return distanceMeasure;
    }

    public void setDistanceMeasure(DistanceMeasure distanceMeasure) {
        this.distanceMeasure = distanceMeasure;
    }

    public int getElementsCount() {
        return elementsCount;
    }

    public void setElementsCount(int elementsCount) {
        this.elementsCount = elementsCount;
    }
}
