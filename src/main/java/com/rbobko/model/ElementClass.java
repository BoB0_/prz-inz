package com.rbobko.model;

import com.rbobko.exception.CloneFailedException;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Super Rafał on 17.04.2017.
 */
@Document
public class ElementClass implements Cloneable{

    @Id
    private String id;
    private int baseClass;
    private int numberOfElementsWithThisClass;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getBaseClass() {
        return baseClass;
    }

    public void setBaseClass(int baseClass) {
        this.baseClass = baseClass;
    }

    public int getNumberOfElementsWithThisClass() {
        return numberOfElementsWithThisClass;
    }

    public void setNumberOfElementsWithThisClass(int numberOfElementsWithThisClass) {
        this.numberOfElementsWithThisClass = numberOfElementsWithThisClass;
    }

    @Override
    public Object clone() {
        try {
            ElementClass elementClass = (ElementClass) super.clone();
            elementClass.setId(new ObjectId().toString());
            return elementClass;
        } catch (CloneNotSupportedException e) {
            throw new CloneFailedException("Error encountered while cloning element");
        }
    }
}
