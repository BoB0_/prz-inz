package com.rbobko.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rbobko.exception.CloneFailedException;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Super Rafał on 17.04.2017.
 */
@Document
public class Data implements Cloneable {

    @Id
    private String id;
    private int centroidsCount;
    @JsonIgnore
    private List<String> attributes;
    @JsonIgnore
    private List<Element> elements;
    @JsonIgnore
    private List<ElementClass> elementClasses;
    @JsonIgnore
    private List<Centroid> centroids;
    @JsonIgnore
    private List<Centroid> oldCentroids;
    @JsonIgnore
    private List<List<Element>> elementsTestGroups;
    @JsonIgnore
    private List<Element> currentTestGroup;
    @JsonIgnore
    private List<Double> errors;
    @JsonIgnore
    private List<Double> testsErrors;
    private Double avgError;
    private Double avgTestError;

    public Data(){
        this.attributes = new ArrayList<>();
        this.elements = new ArrayList<>();
        this.elementClasses = new ArrayList<>();
        this.centroids = new ArrayList<>();
        this.oldCentroids = new ArrayList<>();
        this.elementsTestGroups = new ArrayList<>(new ArrayList<>());
        this.currentTestGroup = new ArrayList<>();
        this.errors = new ArrayList<>();
        this.testsErrors = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCentroidsCount() {
        return centroidsCount;
    }

    public void setCentroidsCount(int centroidsCount) {
        this.centroidsCount = centroidsCount;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

    public List<ElementClass> getElementClasses() {
        return elementClasses;
    }

    public void setElementClasses(List<ElementClass> elementClasses) {
        this.elementClasses = elementClasses;
    }

    public List<Centroid> getCentroids() {
        return centroids;
    }

    public void setCentroids(List<Centroid> centroids) {
        this.centroids = centroids;
    }

    public List<Centroid> getOldCentroids() {
        return oldCentroids;
    }

    public void setOldCentroids(List<Centroid> oldCentroids) {
        this.oldCentroids = oldCentroids;
    }

    public List<List<Element>> getElementsTestGroups() {
        return elementsTestGroups;
    }

    public void setElementsTestGroups(List<List<Element>> elementsTestGroups) {
        this.elementsTestGroups = elementsTestGroups;
    }

    public List<Double> getErrors() {
        return errors;
    }

    public void setErrors(List<Double> errors) {
        this.errors = errors;
    }

    public List<Double> getTestsErrors() {
        return testsErrors;
    }

    public void setTestsErrors(List<Double> testsErrors) {
        this.testsErrors = testsErrors;
    }

    public Double getAvgError() {
        return avgError;
    }

    public void setAvgError(Double avgError) {
        this.avgError = avgError;
    }

    public Double getAvgTestError() {
        return avgTestError;
    }

    public void setAvgTestError(Double avgTestError) {
        this.avgTestError = avgTestError;
    }

    public List<Element> getCurrentTestGroup() {
        return currentTestGroup;
    }

    public void setCurrentTestGroup(List<Element> currentTestGroup) {
        this.currentTestGroup = currentTestGroup;
    }

    @Override
    public Object clone() {
        try {
            Data data = (Data) super.clone();

            data.setId(new ObjectId().toString());
            if(attributes != null){
                data.attributes = new ArrayList<>(this.attributes);
            }
            if(elements != null){
                List<Element> eList = new ArrayList<>();
                elements.forEach(e -> eList.add((Element) e.clone()));
                data.elements = eList;
            }
            if(elementClasses != null){
                List<ElementClass> eClassList = new ArrayList<>();
                elementClasses.forEach(e -> eClassList.add((ElementClass) e.clone()));
                data.elementClasses = eClassList;
            }
            if(centroids != null){
                List<Centroid> cList = new ArrayList<>();
                centroids.forEach(c -> cList.add((Centroid) c.clone()));
                data.centroids = cList;
            }
            data.oldCentroids = new ArrayList<>();
            data.elementsTestGroups = new ArrayList<>(new ArrayList<>());
            data.currentTestGroup = new ArrayList<>();
            data.errors = new ArrayList<>();
            data.testsErrors = new ArrayList<>();
            data.avgError = null;
            data.avgTestError = null;

            return data;
        } catch (CloneNotSupportedException e) {
            throw new CloneFailedException("Error encountered while cloning element");
        }
    }
}
