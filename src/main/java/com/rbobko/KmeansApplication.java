package com.rbobko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KmeansApplication {

	public static void main(String[] args) {
		SpringApplication.run(KmeansApplication.class, args);
	}
}
