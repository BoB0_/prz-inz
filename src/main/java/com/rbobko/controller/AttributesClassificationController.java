package com.rbobko.controller;

import com.rbobko.model.AttributesClassification;
import com.rbobko.model.DistanceMeasure;
import com.rbobko.service.AttributesClassificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

/**
 * Created by Super Rafał on 15.05.2017.
 */
@RestController
@RequestMapping(value = "/api/attributes_classification")
public class AttributesClassificationController {

    private final AttributesClassificationService attributesClassificationService;

    @Autowired
    public AttributesClassificationController(AttributesClassificationService attributesClassificationService) {
        this.attributesClassificationService = attributesClassificationService;
    }

    @CrossOrigin(origins = {"http://localhost:4200"})
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity classifyAttributes(@RequestParam MultipartFile file,
                                             @RequestParam(required = false) int centroids,
                                             @RequestParam(name = "distance_measure", required = false) Optional<Integer> distanceMeasure) {
        AttributesClassification attributesClassification = attributesClassificationService
                .classifyAttributes(file, centroids, distanceMeasure.orElse(-1));
        return ResponseEntity.ok(attributesClassification);
    }
}
