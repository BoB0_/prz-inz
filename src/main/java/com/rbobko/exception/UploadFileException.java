package com.rbobko.exception;

/**
 * Created by Super Rafał on 17.04.2017.
 */
public class UploadFileException extends RuntimeException {

    public UploadFileException(String message){
        super(message);
    }
}
