package com.rbobko.exception;

/**
 * Created by Super Rafał on 11.05.2017.
 */
public class CloneFailedException extends RuntimeException {
    public CloneFailedException(String message){
        super(message);
    }
}
