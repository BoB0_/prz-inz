package com.rbobko.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by Super Rafał on 17.04.2017.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler({IllegalArgumentException.class, RuntimeException.class})
    public ResponseEntity exceptionBadRequest(Exception ex){
        LOGGER.error(ex.getMessage(), ex);
        ExceptionDTO exceptionDTO = new ExceptionDTO(ex.getMessage(), 400);
        return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
    }
}
