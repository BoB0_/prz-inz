package com.rbobko.service;

import com.rbobko.exception.UploadFileException;
import com.rbobko.model.Data;
import com.rbobko.model.Element;
import com.rbobko.model.ElementClass;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by Super Rafał on 17.04.2017.
 */
@Service
public class DataPrepareServiceImpl implements DataPrepareService {

    private final FileReadService fileReadService;

    @Autowired
    public DataPrepareServiceImpl(FileReadService fileReadService) {
        this.fileReadService = fileReadService;
    }

    @Override
    public Data convertFileToData(MultipartFile file) {
        if (!file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1).equals("csv")) {
            throw new IllegalArgumentException("File must be in csv format");
        }

        List<String> lines = getLinesFromFile(file);
        Data data = new Data();
        data.setId(new ObjectId().toString());
        int lineNumber = 0;

        for (String line : lines) {
            convertLine(splitLine(line), lineNumber, data);
            lineNumber++;
        }

        return data;
    }

    private List<String> getLinesFromFile(MultipartFile file) {
        try {
            return fileReadService.readCsvFile(file.getInputStream());
        } catch (IOException e) {
            throw new UploadFileException("Error encountered while getting uploaded file as stream");
        }
    }

    private List<String> splitLine(String line) {
        if (line.contains(";")) {
            return Arrays.asList(line.split(";"));
        } else {
            return Arrays.asList(line.split(","));
        }
    }

    private void convertLine(List<String> values, int lineNumber, Data data) {
        if (lineNumber == 0) {
            for (int i = 0; i < values.size() - 1; i++) {
                data.getAttributes().add(values.get(i));
            }
        } else {
            if (values.size() != data.getAttributes().size() + 1) {
                throw new IllegalArgumentException("Wrong arguments number in line " + lineNumber + 1);
            }
            Element element = new Element();
            element.setId(new ObjectId().toString());
            element.setGivenClass(-1);
            element.setCentroidGroup(-1);
            element.setAttributesValues(new ArrayList<>());

            values.forEach(v -> {
                Double value;
                if(v.contains(",")){
                    try {
                        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
                        Number number = format.parse(v);
                        value = number.doubleValue();
                    } catch (ParseException e){
                        throw new IllegalArgumentException("Wrong argument: " + v + " in line " + lineNumber + 1);
                    }
                }else{
                    value = new Double(v);
                }
                element.getAttributesValues().add(value);
            });

            element.setBaseClass(new Integer(values.get(values.size() - 1)));
            updateClasses(element, data);

            data.getElements().add(element);
        }
    }

    private void updateClasses(Element element, Data data) {
        if (data.getElementClasses() == null) {
            data.setElementClasses(new ArrayList<>());
        }

        ElementClass elementClass = data.getElementClasses().stream().filter(ec -> ec.getBaseClass() == element.getBaseClass()).findAny().orElse(null);

        if (elementClass == null) {
            elementClass = new ElementClass();
            elementClass.setId(new ObjectId().toString());
            elementClass.setBaseClass(element.getBaseClass());
            elementClass.setNumberOfElementsWithThisClass(1);
            data.getElementClasses().add(elementClass);
        } else {
            elementClass.setNumberOfElementsWithThisClass(elementClass.getNumberOfElementsWithThisClass() + 1);
        }
    }
}
