package com.rbobko.service;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Super Rafał on 17.04.2017.
 */
public interface FileReadService {
    List<String> readCsvFile(InputStream inputStream);
}
