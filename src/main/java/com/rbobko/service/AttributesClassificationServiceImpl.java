package com.rbobko.service;

import com.rbobko.model.*;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Super Rafał on 15.05.2017.
 */
@Service
public class AttributesClassificationServiceImpl implements AttributesClassificationService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private final KMeansService kMeansService;
    private final DataPrepareService dataPrepareService;

    @Autowired
    public AttributesClassificationServiceImpl(KMeansService kMeansService, DataPrepareService dataPrepareService) {
        this.kMeansService = kMeansService;
        this.dataPrepareService = dataPrepareService;
    }

    @Override
    public AttributesClassification classifyAttributes(MultipartFile file, int centroids, int distanceMeasure) {
        AttributesClassification attributesClassification = new AttributesClassification();
        attributesClassification.setId(new ObjectId().toString());


        Data basicData = dataPrepareService.convertFileToData(file);
        attributesClassification.setElementsCount(basicData.getElements().size());
        copyAttributes(attributesClassification, basicData);
        attributesClassification.setDataList(generateDataForCentroids(basicData, centroids));
        determineDistanceMeasure(attributesClassification, distanceMeasure);
        executeKMeansForDataList(attributesClassification.getDataList(), attributesClassification.getDistanceMeasure());
        chooseDataWithLowestError(attributesClassification);
        executeAttributesClassification(attributesClassification);
        determineAttributesMagnitude(attributesClassification);

        return attributesClassification;
    }

    private void copyAttributes(AttributesClassification attributesClassification, Data basicData){
        List<Attribute> attributes = new ArrayList<>();

        basicData.getAttributes().forEach(attribute -> {
            Attribute newAttribute = new Attribute();
            newAttribute.setId(new ObjectId().toString());
            newAttribute.setName(attribute);
            newAttribute.setAvgError(-1d);
            newAttribute.setMagnitude(-1d);

            attributes.add(newAttribute);
        });

        attributesClassification.setAttributes(attributes);
    }

    private List<Data> generateDataForCentroids(Data data, int centroids){
        List<Data> dataList = new ArrayList<>();
        for(int i = 0; i < centroids; i++){
            Data generatedData = (Data) data.clone();
            generatedData.setCentroidsCount(i+1);
            generateDataForSpecifiedNumberOfCentroids(generatedData, i+1);
            dataList.add(generatedData);
        }

        return dataList;
    }

    private void generateDataForSpecifiedNumberOfCentroids(Data data, int centroids){
        kMeansService.determineElementsTestGroups(data);
        kMeansService.determineCentroids(data, centroids);
    }

    private void determineDistanceMeasure(AttributesClassification attributesClassification, int distanceMeasure){
        attributesClassification.setDistanceMeasure(DistanceMeasure.create(distanceMeasure));
    }

    private void executeKMeansForDataList(List<Data> dataList, DistanceMeasure distanceMeasure){
        dataList.forEach(data -> {
            LOGGER.info("Executing kmeans for {} centroids", data.getCentroidsCount());
            kMeansService.executeKMeans(data, distanceMeasure);
        });
    }

    private void chooseDataWithLowestError(AttributesClassification attributesClassification){
        double minError = 101d;

        for (Data data : attributesClassification.getDataList()) {
            //TODO Ask about this

            double error = (data.getAvgError() + data.getAvgTestError())/2;
            if(error < minError){
                minError = error;
                attributesClassification.setChosenData((Data) data.clone());
                attributesClassification.getChosenData().setAvgError(data.getAvgError());
                attributesClassification.getChosenData().setAvgTestError(data.getAvgTestError());
            }
        }
        LOGGER.info("Data with lowest error chosen");
    }

    private void executeAttributesClassification(AttributesClassification attributesClassification){
        for(int i = 0; i < attributesClassification.getAttributes().size(); i++){
            LOGGER.info("Mixing values for {} attribute", attributesClassification.getAttributes().get(i).getName());
            Data chosenData = (Data) attributesClassification.getChosenData().clone();
            List<Double> attributeValues = new ArrayList<>();

            for (Element element : chosenData.getElements()) {
                attributeValues.add(element.getAttributesValues().get(i));
            }

            for (Element element : chosenData.getElements()) {
                int valueIndex = ThreadLocalRandom.current().nextInt(0, attributeValues.size());
                element.getAttributesValues().set(i, attributeValues.get(valueIndex));
                attributeValues.remove(valueIndex);
            }

            generateDataForSpecifiedNumberOfCentroids(chosenData,
                    chosenData.getCentroids().size()/chosenData.getElementClasses().size());
            executeKMeansForDataList(Collections.singletonList(chosenData), attributesClassification.getDistanceMeasure());

            //TODO Ask about this
            double error = (chosenData.getAvgError()+chosenData.getAvgTestError())/2;
            attributesClassification.getAttributes().get(i).setAvgError(error);
        }
    }

    private void determineAttributesMagnitude(AttributesClassification attributesClassification){
        Double maximumAvgError = -1d;
        for (Attribute attribute : attributesClassification.getAttributes()) {
            if(attribute.getAvgError() > maximumAvgError){
                maximumAvgError = attribute.getAvgError();
            }
        }

        for(Attribute attribute : attributesClassification.getAttributes()){
            attribute.setMagnitude((attribute.getAvgError()*100)/maximumAvgError);
        }
    }
}
