package com.rbobko.service;

import com.rbobko.model.*;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * Created by Super Rafał on 17.04.2017.
 */
@Service
public class KMeansServiceImpl implements KMeansService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private final DistanceMeasureService distanceMeasureService;

    @Autowired
    public KMeansServiceImpl(DistanceMeasureService distanceMeasureService) {
        this.distanceMeasureService = distanceMeasureService;
    }

    @Override
    public void determineElementsTestGroups(Data data) {
        determineTestGroupWithProportionPreseved(data);
    }

    private void determineTestGroupsWithoutProportionPreserved(Data data) {
        List<List<Element>> elementsTestGroups = new ArrayList<>();
        int groupSize = data.getElements().size() / 10;
        int sublistStartIndex = 0;
        int sublistEndIndex = groupSize;

        for (int i = 0; i < 10; i++) {
            if (i == 9) {
                sublistEndIndex = data.getElements().size();
            }
            List<Element> elementTestGroup = data.getElements().subList(sublistStartIndex, sublistEndIndex);
            elementsTestGroups.add(elementTestGroup);
            sublistStartIndex = sublistEndIndex;
            sublistEndIndex += groupSize;
        }

        data.setElementsTestGroups(elementsTestGroups);
    }

    private void determineTestGroupWithProportionPreseved(Data data) {
        List<List<Element>> elementsTestGroups = new ArrayList<>();

        List<Element> elementList = new ArrayList<>();
        data.getElements().forEach(e -> elementList.add((Element) e.clone()));

        List<ElementClass> elementClassesNumberForEachTestGroup = new ArrayList<>();
        data.getElementClasses().forEach(ec -> {
            ElementClass elementClass = new ElementClass();
            elementClass.setId(ec.getId());
            elementClass.setBaseClass(ec.getBaseClass());
            elementClass.setNumberOfElementsWithThisClass(ec.getNumberOfElementsWithThisClass() / 10);

            elementClassesNumberForEachTestGroup.add(elementClass);
        });

        for (int i = 0; i < 9; i++) {
            List<Element> elementTestGroup = new ArrayList<>();

            elementClassesNumberForEachTestGroup.forEach(ec -> {
                List<Element> classElements = elementList.stream().filter(e -> e.getBaseClass() == ec.getBaseClass()).collect(Collectors.toList());

                for (int j = 0; j < ec.getNumberOfElementsWithThisClass(); j++) {
                    Element element = classElements.get(ThreadLocalRandom.current().nextInt(0, classElements.size()));
                    elementTestGroup.add(element);
                    elementList.remove(element);
                    classElements.remove(element);
                }
            });
            elementsTestGroups.add(elementTestGroup);
        }

        elementsTestGroups.add(elementList);

        data.setElementsTestGroups(elementsTestGroups);
    }

    @Override
    public void determineCentroids(Data data, int centroidsCount) {
        determineCentroidsPlacedOnElements(data, centroidsCount);
    }

    /**
     * CENTROIDS GENERATION BASED ON ELEMENTS
     */
    private void determineCentroidsPlacedOnElements(Data data, int centroidsCount) {
        List<Centroid> centroids = new ArrayList<>();
        int group = 0;

        for (int i = 0; i < data.getElementClasses().size(); i++) {
            int finalI = i;
            List<Element> classElements = data.getElements().stream()
                    .filter(element -> element.getBaseClass() == data.getElementClasses().get(finalI).getBaseClass())
                    .collect(Collectors.toList());

            List<Centroid> classCentroids = new ArrayList<>();

            for (int j = 0; j < centroidsCount; j++) {
                if (classElements.isEmpty()) {
                    continue;
                }

                Element element = classElements.get(ThreadLocalRandom.current().nextInt(0, classElements.size()));
                Centroid centroid = new Centroid();
                centroid.setId(new ObjectId().toString());
                centroid.setAttributesValues(new ArrayList<>());
                element.getAttributesValues().forEach(av -> centroid.getAttributesValues().add(new Double(av.doubleValue())));
                centroid.setBaseClass(element.getBaseClass());
                centroid.setGroup(group);

                classElements.remove(element);
                classCentroids.add(centroid);
                group++;
            }

            centroids.addAll(classCentroids);
        }

        data.setCentroids(centroids);
    }

    @Override
    public void executeKMeans(Data data, DistanceMeasure distanceMeasure) {
        List<Centroid> initializedCentroids = data.getCentroids();
        int testGroupCount = 0;

        for (List<Element> testGroup : data.getElementsTestGroups()) {
            testGroupCount++;
            data.setCurrentTestGroup(testGroup);
            data.setCentroids(initializedCentroids);
            data.getOldCentroids().clear();
            while (hasCentroidsChanged(data)) {
                rememberCentroids(data);
                resetElementsGroup(data);
                determineElementsGroup(data, distanceMeasure);
                determineNewCentroidsAttributesValues(data);
            }
            determineError(data);
            determineTestError(data, distanceMeasure);
            LOGGER.info("Done executing kmeans for {} centroids, test group {}", data.getCentroids().size() / data.getElementClasses().size(), testGroupCount);
        }
        determineAverageError(data);
    }

    private void rememberCentroids(Data data) {
        List<Centroid> oldCentroids = new ArrayList<>();
        data.getCentroids().forEach(centroid -> {
            Centroid c = (Centroid) centroid.clone();
            oldCentroids.add(c);
        });

        data.setOldCentroids(oldCentroids);
    }

    private void resetElementsGroup(Data data) {
        data.getElements().forEach(element -> element.setCentroidGroup(-1));
    }

    private void determineElementsGroup(Data data, DistanceMeasure distanceMeasure) {
        data.getElements().forEach(element -> {
            if (!data.getCurrentTestGroup().contains(element)) {
                determineElementGroup(element, data.getCentroids(), distanceMeasure);
            }
        });
    }

    private void determineElementGroup(Element element, List<Centroid> centroids, DistanceMeasure distanceMeasure) {
        Double minDistance = Double.MAX_VALUE;

        for (Centroid centroid : centroids) {

            Double distance = distanceMeasureService.getDistance(element, centroid, distanceMeasure);

            if (distance < minDistance) {
                element.setGivenClass(centroid.getBaseClass());
                element.setCentroidGroup(centroid.getGroup());
                minDistance = distance;
            }
        }
    }

    private void determineNewCentroidsAttributesValues(Data data) {
        data.getCentroids().forEach(centroid -> {
            List<Element> elements = data.getElements().stream()
                    .filter(element -> element.getCentroidGroup() == centroid.getGroup())
                    .collect(Collectors.toList());

            if (!elements.isEmpty()) {
                List<Double> values = new ArrayList<>(
                        Collections.nCopies(centroid.getAttributesValues().size(), 0d));

                elements.forEach(element -> {
                    for (int i = 0; i < element.getAttributesValues().size(); i++) {
                        values.set(i, values.get(i) + element.getAttributesValues().get(i));
                    }
                });

                for (int i = 0; i < values.size(); i++) {
                    values.set(i, values.get(i) / elements.size());
                }

                centroid.setAttributesValues(values);
            }
        });
    }

    private boolean hasCentroidsChanged(Data data) {
        if (data.getOldCentroids().isEmpty()) {
            return true;
        }

        for (Centroid centroid : data.getCentroids()) {
            Centroid oldCentroid = data.getOldCentroids().stream()
                    .filter(c -> c.getId().equals(centroid.getId())).findAny().orElse(null);

            if (oldCentroid != null) {
                if (!centroid.equals(oldCentroid)) {
                    return true;
                }
            }
        }

        return false;
    }

    private void determineError(Data data) {
        if (data.getErrors() == null) {
            data.setErrors(new ArrayList<>());
        }

        int errorsCount = 0;

        for (Element element : data.getElements()) {
            if (element.getCentroidGroup() != -1) {
                if (element.getGivenClass() != element.getBaseClass()) {
                    errorsCount++;
                }
            }
        }

        double averageError = (errorsCount * 100) / (data.getElements().size() - data.getCurrentTestGroup().size());

        data.getErrors().add(averageError);
    }

    private void determineTestError(Data data, DistanceMeasure distanceMeasure) {
        if (data.getTestsErrors() == null) {
            data.setTestsErrors(new ArrayList<>());
        }

        final int[] errorsCount = {0};

        data.getCurrentTestGroup().forEach(element -> {
            determineElementGroup(element, data.getCentroids(), distanceMeasure);

            if (element.getBaseClass() != element.getGivenClass()) {
                errorsCount[0]++;
            }
        });

        double testError = (errorsCount[0] * 100) / (data.getCurrentTestGroup().size());

        data.getTestsErrors().add(testError);
    }

    private void determineAverageError(Data data) {
        double error = 0;

        for (Double e : data.getErrors()) {
            error += e;
        }
        data.setAvgError(error / data.getErrors().size());
        error = 0;

        for (Double e : data.getTestsErrors()) {
            error += e;
        }
        data.setAvgTestError(error / data.getTestsErrors().size());
    }
}
