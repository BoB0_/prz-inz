package com.rbobko.service;

import com.rbobko.model.Centroid;
import com.rbobko.model.DistanceMeasure;
import com.rbobko.model.Element;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Super Rafał on 12.06.2017.
 */
@Service
public class DistanceMeasureServiceImpl implements DistanceMeasureService {

    @Override
    public double getDistance(Element element, Centroid centroid, DistanceMeasure distanceMeasure) {
        Double distance;

        switch(distanceMeasure){
            case EUKLIDES:
                distance = getEuklidesDistance(element, centroid);
                break;
            case CANBERR:
                distance = getCanberrDistance(element, centroid);
                break;
            case CZYBYSZEW:
                distance = getCzybyszewDistance(element, centroid);
                break;
            case MANHATTAN:
                distance = getManhattanDistance(element, centroid);
                break;
            default:
                distance = getEuklidesDistance(element, centroid);
                break;
        }

        return distance;
    }

    private double getEuklidesDistance(Element element, Centroid centroid){
        Double distance = 0d;

        for(int i = 0; i < element.getAttributesValues().size(); i++){
            distance += Math.pow(element.getAttributesValues().get(i)-
                    centroid.getAttributesValues().get(i), 2);
        }

        return Math.sqrt(distance);
    }

    private double getCanberrDistance(Element element, Centroid centroid){
        Double distance = 0d;

        for(int i = 0; i < element.getAttributesValues().size(); i++){
            distance += Math.abs((element.getAttributesValues().get(i) - centroid.getAttributesValues().get(i)) / (element.getAttributesValues().get(i) + centroid.getAttributesValues().get(i)));
        }

        return distance;
    }

    private double getCzybyszewDistance(Element element, Centroid centroid){
        List<Double> distanceList = new ArrayList<>();

        for(int i = 0; i < element.getAttributesValues().size(); i++){
            distanceList.add(Math.abs(element.getAttributesValues().get(i) - centroid.getAttributesValues().get(i)));
        }

        return Collections.max(distanceList);
    }

    private double getManhattanDistance(Element element, Centroid centroid){
        Double distance = 0d;

        for(int i = 0; i < element.getAttributesValues().size(); i++){
            distance += Math.abs(element.getAttributesValues().get(i) - centroid.getAttributesValues().get(i));
        }

        return distance;
    }
}
