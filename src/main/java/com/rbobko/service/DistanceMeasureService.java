package com.rbobko.service;

import com.rbobko.model.Centroid;
import com.rbobko.model.DistanceMeasure;
import com.rbobko.model.Element;

/**
 * Created by Super Rafał on 12.06.2017.
 */
public interface DistanceMeasureService {
    double getDistance(Element element, Centroid centroid, DistanceMeasure distanceMeasure);
}
