package com.rbobko.service;

import com.rbobko.model.AttributesClassification;
import com.rbobko.model.DistanceMeasure;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Super Rafał on 15.05.2017.
 */
public interface AttributesClassificationService {
    AttributesClassification classifyAttributes(MultipartFile file, int centroids, int distanceMeasure);
}
