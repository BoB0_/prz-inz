package com.rbobko.service;

import com.rbobko.exception.UploadFileException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Super Rafał on 17.04.2017.
 */
@Service
public class FileReadServiceImpl implements FileReadService {

    @Override
    public List<String> readCsvFile(InputStream inputStream){
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            List<String> fileLines = reader.lines().collect(Collectors.toList());
            reader.close();
            return fileLines;
        }catch (IOException e){
            throw new UploadFileException(e.getMessage());
        }
    }
}