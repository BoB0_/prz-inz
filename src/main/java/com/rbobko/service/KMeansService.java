package com.rbobko.service;

import com.rbobko.model.Data;
import com.rbobko.model.DistanceMeasure;

/**
 * Created by Super Rafał on 17.04.2017.
 */
public interface KMeansService {
    void executeKMeans(Data data, DistanceMeasure distanceMeasure);
    void determineElementsTestGroups(Data data);
    void determineCentroids(Data data, int centroidsCount);
}
