package com.rbobko.service;

import com.rbobko.model.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Super Rafał on 17.04.2017.
 */
public interface DataPrepareService {
    Data convertFileToData(MultipartFile file);
}
